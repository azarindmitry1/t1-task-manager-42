package ru.t1.azarin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @Column
    @NotNull
    private Date date = new Date();

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
