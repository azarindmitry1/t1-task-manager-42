package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.service.*;
import ru.t1.azarin.tm.exception.entity.UserNotFoundException;
import ru.t1.azarin.tm.exception.field.EmailEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.LoginEmptyException;
import ru.t1.azarin.tm.exception.field.PasswordEmptyException;
import ru.t1.azarin.tm.exception.user.ExistEmailException;
import ru.t1.azarin.tm.exception.user.ExistLoginException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.dto.model.UserDTO;

import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectService PROJECT_SERVICE = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(
            PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE
    );

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "TEST_STRING";

    @Before
    public void before() {
        USER_SERVICE.add(USER1);
    }

    @After
    public void after() {
        if (USER_SERVICE.findOneById(USER1.getId()) != null)
            USER_SERVICE.remove(USER1);
    }

    @Test
    public void create() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.create(emptyString, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.create(nullString, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(ExistLoginException.class, () ->
                USER_SERVICE.create(USER1.getLogin(), USER1.getPasswordHash(), USER1.getEmail())
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                USER_SERVICE.create(USER_UNREGISTRY_LOGIN, emptyString, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                USER_SERVICE.create(USER_UNREGISTRY_LOGIN, nullString, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(ExistEmailException.class, () ->
                USER_SERVICE.create(USER_UNREGISTRY_LOGIN, USER_UNREGISTRY_PASSWORD, USER1.getEmail())
        );
        @Nullable final UserDTO user = USER_SERVICE.create(
                USER_UNREGISTRY_LOGIN, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL
        );
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_UNREGISTRY_LOGIN, user.getLogin());
        Assert.assertEquals(USER_UNREGISTRY_EMAIL, user.getEmail());
        Assert.assertEquals(USER_SERVICE.findOneById(user.getId()).getId(), user.getId());
        USER_SERVICE.remove(user);
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.findByLogin(emptyString)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.findByLogin(nullString)
        );
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1.getLogin(), user.getLogin());
    }

    @Test
    public void findByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () ->
                USER_SERVICE.findByEmail(emptyString)
        );
        Assert.assertThrows(EmailEmptyException.class, () ->
                USER_SERVICE.findByEmail(nullString)
        );
        @Nullable final UserDTO user = USER_SERVICE.findByEmail(USER1.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1.getEmail(), user.getEmail());
    }

    @Test
    public void isLoginExist() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.isLoginExist(emptyString));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.isLoginExist(nullString));
        Assert.assertTrue(USER_SERVICE.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(USER_SERVICE.isLoginExist(USER_UNREGISTRY_LOGIN));
    }

    @Test
    public void isEmailExist() {
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.isEmailExist(emptyString));
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.isEmailExist(nullString));
        Assert.assertTrue(USER_SERVICE.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(USER_SERVICE.isEmailExist(USER_UNREGISTRY_EMAIL));
    }

    @Test
    public void remove() {
        @Nullable final UserDTO user = USER_SERVICE.findOneById(USER1.getId());
        USER_SERVICE.remove(user);
        Assert.assertNull(USER_SERVICE.findOneById(USER1.getId()));
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.removeByLogin(emptyString)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.removeByLogin(nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                USER_SERVICE.removeByLogin(USER_UNREGISTRY_LOGIN)
        );
        USER_SERVICE.removeByLogin(USER1.getLogin());
        Assert.assertNull(USER_SERVICE.findOneById(USER1.getId()));
    }

    @Test
    public void removeByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () ->
                USER_SERVICE.removeByEmail(emptyString)
        );
        Assert.assertThrows(EmailEmptyException.class, () ->
                USER_SERVICE.removeByEmail(nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                USER_SERVICE.removeByEmail(USER_UNREGISTRY_EMAIL)
        );
        USER_SERVICE.removeByEmail(USER1_EMAIL);
        Assert.assertNull(USER_SERVICE.findOneById(USER1.getId()));
    }

    @Test
    public void setPassword() {
        Assert.assertThrows(IdEmptyException.class, () ->
                USER_SERVICE.setPassword(emptyString, USER_UNREGISTRY_PASSWORD)
        );
        Assert.assertThrows(IdEmptyException.class, () ->
                USER_SERVICE.setPassword(nullString, USER_UNREGISTRY_PASSWORD)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                USER_SERVICE.setPassword(USER_UNREGISTRY_ID, emptyString)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                USER_SERVICE.setPassword(USER_UNREGISTRY_ID, nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                USER_SERVICE.setPassword(USER_UNREGISTRY_ID, USER_UNREGISTRY_PASSWORD)
        );
        USER_SERVICE.setPassword(USER1.getId(), ADMIN_PASSWORD);
        @NotNull final UserDTO user = USER_SERVICE.findOneById(USER1.getId());
        Assert.assertEquals(ADMIN.getPasswordHash(), user.getPasswordHash());
        USER_SERVICE.setPassword(user.getId(), USER1_PASSWORD);
    }

    @Test
    public void updateUser() {
        Assert.assertThrows(IdEmptyException.class, () ->
                USER_SERVICE.updateUser(emptyString, testString, testString, testString)
        );
        Assert.assertThrows(IdEmptyException.class, () ->
                USER_SERVICE.updateUser(nullString, testString, testString, testString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                USER_SERVICE.updateUser(USER_UNREGISTRY_ID, testString, testString, testString)
        );
        @Nullable final UserDTO user = USER_SERVICE.findOneById(USER1.getId());
        USER_SERVICE.updateUser(USER1.getId(), testString, testString, testString);
        Assert.assertEquals(USER1.getFirstName(), user.getFirstName());
        Assert.assertEquals(USER1.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(USER1.getLastName(), user.getLastName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.lockUserByLogin(emptyString)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.lockUserByLogin(nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                USER_SERVICE.lockUserByLogin(USER_UNREGISTRY_ID)
        );
        USER_SERVICE.lockUserByLogin(USER1.getLogin());
        @NotNull final UserDTO user = USER_SERVICE.findOneById(USER1.getId());
        Assert.assertTrue(user.isLocked());
        USER_SERVICE.unlockUserByLogin(USER1.getLogin());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.unlockUserByLogin(emptyString)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                USER_SERVICE.unlockUserByLogin(nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                USER_SERVICE.unlockUserByLogin(USER_UNREGISTRY_ID)
        );
        @NotNull final UserDTO user = USER_SERVICE.findOneById(USER1.getId());
        USER_SERVICE.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(user.isLocked());
    }

}
