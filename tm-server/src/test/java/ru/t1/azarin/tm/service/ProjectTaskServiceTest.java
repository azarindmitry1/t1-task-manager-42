package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.service.*;
import ru.t1.azarin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.azarin.tm.exception.field.TaskIdEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.dto.model.TaskDTO;
import ru.t1.azarin.tm.dto.model.UserDTO;
import ru.t1.azarin.tm.util.HashUtil;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT2;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK1;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK2;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectService PROJECT_SERVICE = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(
            PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE
    );

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        USER_SERVICE.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER1_LOGIN);
        USER_SERVICE.remove(user);
    }

    @Before
    public void before() {
        USER1_PROJECT1.setUserId(USER_ID);
        USER1_PROJECT2.setUserId(USER_ID);
        USER1_TASK1.setUserId(USER_ID);
        USER1_TASK2.setUserId(USER_ID);
        PROJECT_SERVICE.add(USER1_PROJECT1);
        PROJECT_SERVICE.add(USER1_PROJECT2);
        TASK_SERVICE.add(USER1_TASK1);
        TASK_SERVICE.add(USER1_TASK2);
    }

    @After
    public void after() {
        TASK_SERVICE.clear(USER_ID);
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                emptyString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                nullString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                USER1.getId(), emptyString, USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                USER1.getId(), nullString, USER1_TASK1.getId())
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                USER1.getId(), USER1_PROJECT1.getId(), emptyString)
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                USER1.getId(), USER1_PROJECT1.getId(), nullString)
        );
        projectTaskService.bindTaskToProject(USER_ID, USER1_PROJECT1.getId(), USER1_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                emptyString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                nullString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                USER_ID, emptyString, USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                USER_ID, nullString, USER1_TASK1.getId())
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                USER_ID, USER1_PROJECT1.getId(), emptyString)
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                USER_ID, USER1_PROJECT1.getId(), nullString)
        );
        projectTaskService.unbindTaskToProject(USER_ID, USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(
                emptyString, USER1_PROJECT1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(
                nullString, USER1_PROJECT1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(
                USER_ID, emptyString)
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(
                USER_ID, nullString)
        );
        projectTaskService.removeProjectById(USER_ID, USER1_PROJECT2.getId());
        Assert.assertNull(PROJECT_SERVICE.findOneById(USER_ID, USER1_PROJECT2.getId()));
    }

}
