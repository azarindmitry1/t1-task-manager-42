package ru.t1.azarin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.dto.model.SessionDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static SessionDTO USER1_SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO ADMIN_SESSION1 = new SessionDTO();

    @NotNull
    public final static List<SessionDTO> USER1_SESSION_LIST = Arrays.asList(USER1_SESSION1);

    @NotNull
    public final static List<SessionDTO> ADMIN_SESSION_LIST = Arrays.asList(ADMIN_SESSION1);

    @NotNull
    public final static List<SessionDTO> SESSION_LIST = new ArrayList<>();

    static {
        USER1_SESSION1.setRole(Role.USUAL);
        USER1_SESSION1.setDate(new Date());
    }

}
