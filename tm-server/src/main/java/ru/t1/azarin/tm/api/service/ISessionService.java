package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO add(@Nullable String userId, @Nullable SessionDTO session);

    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

}
