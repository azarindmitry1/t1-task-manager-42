package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IServiceLocator;
import ru.t1.azarin.tm.api.service.IUserService;
import ru.t1.azarin.tm.dto.request.user.*;
import ru.t1.azarin.tm.dto.response.user.*;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.dto.model.SessionDTO;
import ru.t1.azarin.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.azarin.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changePasswordResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String newPassword = request.getNewPassword();
        getUserService().setPassword(userId, newPassword);
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable final UserDTO user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateProfileResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String lastName = request.getLastName();
        getUserService().updateUser(userId, firstName, middleName, lastName);
        return new UserUpdateProfileResponse();
    }

}
