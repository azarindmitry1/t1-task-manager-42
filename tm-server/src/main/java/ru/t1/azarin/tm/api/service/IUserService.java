package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserService {

    @NotNull
    UserDTO add(@Nullable UserDTO user);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    List<UserDTO> findAll();

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO findOneById(@Nullable String id);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    void remove(@Nullable UserDTO model);

    void removeByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    void updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    );

}
