package ru.t1.azarin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password_hash, first_name, middle_name, last_name, locked, role, email) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{firstName}, #{middleName}, #{lastName}, #{locked}, " +
            "#{role}, #{email})")
    void add(@NotNull UserDTO user);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Nullable
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    List<UserDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    UserDTO findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    UserDTO findByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    UserDTO findByEmail(@NotNull @Param("email") String email);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@NotNull UserDTO user);

    @Update("UPDATE tm_user SET login = #{login}, password_hash = #{passwordHash}, first_name = #{firstName}, " +
            "middle_name = #{middleName}, last_name = #{lastName}, locked = #{locked}, role = #{role}, " +
            "email = #{email} WHERE id = #{id}")
    void update(@NotNull UserDTO user);

}
