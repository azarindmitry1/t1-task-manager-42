package ru.t1.azarin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, user_id, name, description, status, created) " +
            "VALUES (#{id}, #{userId}, #{name}, #{description}, #{status}, #{created})")
    void add(@NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} and id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} and id = #{id}")
    void remove(@NotNull ProjectDTO project);

    @Update("UPDATE tm_project SET name = #{name}, description = #{description}, status = #{status} WHERE id = #{id}")
    void update(@NotNull ProjectDTO project);

}