package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO add(@Nullable ProjectDTO project);

    void changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable ProjectDTO project);

    void removeById(@Nullable String userId, @Nullable String id);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}