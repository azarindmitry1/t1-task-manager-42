package ru.t1.azarin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.dto.model.UserDTO;

import java.sql.SQLException;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public final static String NAME = "user-view-profile";

    @NotNull
    public final static String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() throws SQLException {
        System.out.println("[VIEW USER PROFILE]");
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest();
        @NotNull final UserDTO user = getAuthEndpoint().viewProfile(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
