package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.project.*;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.response.project.*;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.dto.model.ProjectDTO;
import ru.t1.azarin.tm.service.PropertyService;

import java.sql.SQLException;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @Nullable
    private static String ADMIN_TOKEN;

    @NotNull
    private final String testString = "TEST_STRING";

    @NotNull
    private final String testString2 = "TEST_STRING2";

    @NotNull
    private final String testString3 = "TEST_STRING3";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @Nullable
    private ProjectDTO adminProject1;

    @Nullable
    private ProjectDTO adminProject2;

    @BeforeClass
    public static void setUp() throws SQLException {
        ADMIN_TOKEN = AUTH_ENDPOINT.login(new UserLoginRequest("admin", "admin")).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(request);
    }

    @Before
    public void before() {
        @NotNull final ProjectCreateRequest createRequest1 = new ProjectCreateRequest(ADMIN_TOKEN);
        createRequest1.setName(testString);
        createRequest1.setDescription(testString);
        @NotNull final ProjectCreateResponse createResponse1 = projectEndpoint.createResponse(createRequest1);
        adminProject1 = createResponse1.getProject();

        @NotNull final ProjectCreateRequest createRequest2 = new ProjectCreateRequest(ADMIN_TOKEN);
        createRequest2.setName(testString2);
        createRequest2.setDescription(testString2);
        @NotNull final ProjectCreateResponse createResponse2 = projectEndpoint.createResponse(createRequest2);
        adminProject2 = createResponse2.getProject();
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(ADMIN_TOKEN);
        projectEndpoint.clearResponse(clearRequest);
    }

    @Test
    public void changeStatusById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest(nullString))
        );
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(ADMIN_TOKEN);
        request.setId(adminProject2.getId());
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final ProjectChangeStatusByIdResponse response = projectEndpoint.changeStatusByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void clear() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest(nullString))
        );
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(ADMIN_TOKEN);
        projectEndpoint.clearResponse(request);
        Assert.assertNull(projectEndpoint.listResponse(new ProjectListRequest(ADMIN_TOKEN)).getProjects());
    }

    @Test
    public void completeById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest(nullString))
        );

        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(ADMIN_TOKEN);
        request.setId(adminProject1.getId());
        @Nullable final ProjectCompleteByIdResponse response = projectEndpoint.completeByIdResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

    @Test
    public void create() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest(nullString))
        );
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(ADMIN_TOKEN);
        createRequest.setName(testString3);
        createRequest.setDescription(testString3);
        @Nullable final ProjectCreateResponse createResponse = projectEndpoint.createResponse(createRequest);
        Assert.assertEquals(testString3, createResponse.getProject().getName());
        Assert.assertEquals(testString3, createResponse.getProject().getDescription());
    }

    @Test
    public void list() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest(nullString))
        );
        @Nullable final ProjectListResponse response = projectEndpoint.listResponse(new ProjectListRequest(ADMIN_TOKEN));
        Assert.assertNotNull(response.getProjects());
        Assert.assertEquals(2, response.getProjects().size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest(nullString))
        );
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(ADMIN_TOKEN);
        request.setId(adminProject1.getId());
        @Nullable final ProjectRemoveByIdResponse response = projectEndpoint.removeByIdResponse(request);
        Assert.assertEquals(adminProject1.getId(), response.getProject().getId());
    }

    @Test
    public void startById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest(nullString))
        );
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(ADMIN_TOKEN);
        request.setId(adminProject1.getId());
        @Nullable final ProjectStartByIdResponse response = projectEndpoint.startByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest(nullString))
        );
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(ADMIN_TOKEN);
        request.setId(adminProject1.getId());
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final ProjectUpdateByIdResponse response = projectEndpoint.updateByIdResponse(request);
        Assert.assertEquals(testString3, response.getProject().getName());
        Assert.assertEquals(testString3, response.getProject().getDescription());
    }

}
